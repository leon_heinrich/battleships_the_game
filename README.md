# Battleships_Game (= Schiffe versenken)

The user plays against the computer on a JavaFX GUI and the game is built up of several scenes. The player wins if every ship of the opponent is destroyed.

You can find a more complex description of the project (in german)
[**here**](Systembeschreibung_Battleships.pdf).

## Game process

Menu:

![](https://gitlab.com/leon_heinrich/battleships_the_game/-/raw/master/application_images/menu.png)

Ship placement:

![](https://gitlab.com/leon_heinrich/battleships_the_game/-/raw/master/application_images/ship_placement_1.png)
![](https://gitlab.com/leon_heinrich/battleships_the_game/-/raw/master/application_images/ship_placement_2.png)

Switching to next phase:

![](https://gitlab.com/leon_heinrich/battleships_the_game/-/raw/master/application_images/switch_to_battlephase.png)

Battle phase:

![](https://gitlab.com/leon_heinrich/battleships_the_game/-/raw/master/application_images/battlephase_1.png)
![](https://gitlab.com/leon_heinrich/battleships_the_game/-/raw/master/application_images/battlephase_2.png)

Game finished:

![](https://gitlab.com/leon_heinrich/battleships_the_game/-/raw/master/application_images/game_won.png)
